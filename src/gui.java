/*
   Sistemi Embedded e IoT 2020-2021
   Assignment #2: Smart Experiment
   Author:
   - Andrea Casadei 800898
*/

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.*;
import javax.swing.JButton;

public class gui {

    static SerialCommChannel channel;
    static String s, d, t, v, a = "";
    static JButton btnEndExp;
    static JLabel lblTitle, lblDistance, lblSpeed, lblAcceleration;
    static JLabel[] labels = new JLabel[4];

    public static void main(String[] args) {
        createAndShowGUI();

        while (true) {
            try {
                communicate();
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    private static void createAndShowGUI() {
        JFrame frame = new JFrame();
        frame.setBounds(100, 100, 450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("Smart Experiment");
        frame.setLocationByPlatform(true);
        frame.setVisible(true);

        lblTitle = new JLabel("Title");
        lblTitle.setBounds(10, 11, 47, 14);
        frame.getContentPane().add(lblTitle);

        lblDistance = new JLabel("Distance");
        lblDistance.setBounds(10, 60, 47, 14);
        frame.getContentPane().add(lblDistance);

        lblSpeed = new JLabel("Speed");
        lblSpeed.setBounds(184, 60, 47, 14);
        frame.getContentPane().add(lblSpeed);

        lblAcceleration = new JLabel("Acceleration");
        lblAcceleration.setBounds(379, 60, 47, 14);
        frame.getContentPane().add(lblAcceleration);

        labels[0] = lblTitle;
        labels[1] = lblDistance;
        labels[2] = lblSpeed;
        labels[3] = lblAcceleration;

        btnEndExp = new JButton("End Exp");
        btnEndExp.setBounds(337, 7, 89, 23);
        frame.getContentPane().add(btnEndExp);
        btnEndExp.setEnabled(false);

        btnEndExp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                channel.sendMsg("pressEnd");
            }
        });
    }

    private static void communicate() throws Exception {

        channel = new SerialCommChannel("COM3", 9600);
        String msg = channel.receiveMsg();
        String[] parts = msg.split(":");
        String mode = parts[0];
        String message = parts[1];

        switch (mode) {
            case "5":
                String[] ss = message.split("|");
                lblTitle.setText(ss[0]);
                lblDistance.setText(ss[1]);
                t = ss[2];
                lblSpeed.setText(ss[3]);
                lblAcceleration.setText(ss[4]);
                break;
            case "7":
                lblTitle.setText(message);
                btnEndExp.setEnabled(true);
                break;
            default:
                lblTitle.setText(message);
                break;
        }
    }
}
